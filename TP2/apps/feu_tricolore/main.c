#include "core/system.h"
#include "core/systick.h"
#include "core/pio.h"
#include "drivers/adc.h"
#include "drivers/serial.h"
#include "extdrv/status_led.h"
#include "lib/stdio.h"
#include "drivers/gpio.h"

#define MODULE_VERSION    0x04
#define MODULE_NAME "Feu Tricolore"


#define SELECTED_FREQ  FREQ_SEL_48MHz

/***************************************************************************** */

/**
 * Définition des PINs utilisés pour les LEDs de status(requis)
 **/
const struct pio status_led_green = LPC_GPIO_1_4;
const struct pio status_led_red = LPC_GPIO_1_5;

/**
 * Définition des PINs utilisés
 **/
const struct pio green_light = LPC_GPIO_0_23;
const struct pio orange_light = LPC_GPIO_0_24;
const struct pio red_light = LPC_GPIO_0_25;

/***************************************************************************** */

/**
 * Initialise les pins GPIO
 **/
void init_lights() {
	//void config_gpio(const struct pio* gpio, uint32_t mode, uint8_t dir, uint8_t ini_val);
	// ini_val: valeur par défaut
	config_gpio(&green_light, 0, GPIO_DIR_OUT, 0);
	config_gpio(&orange_light, 0, GPIO_DIR_OUT, 0);
	config_gpio(&red_light, 0, GPIO_DIR_OUT, 0);
}

/**
 * Procédure éclairant les feux
 * 	- Vert
 *	- Orange
 *	- Rouge
 **/
void traffic_lights() {

	gpio_set(green_light);
	msleep(2000);
	
	gpio_clear(green_light);
	gpio_set(orange_light);
	msleep(1000);
	
	gpio_clear(orange_light);
	gpio_set(red_light);
	msleep(3000);

	gpio_clear(red_light);

}

/***************************************************************************** */

/**
 * Initialisation obligatoire
 * pour permettre au système de
 * foncitonner correctement
 **/
void system_init()
{
	/* Stop the watchdog */
	startup_watchdog_disable(); /* Do it right now, before it gets a chance to break in */
	system_set_default_power_state();
	clock_config(SELECTED_FREQ);
	

	status_led_config(&status_led_green, &status_led_red);
	/* System tick timer MUST be configured and running in order to use the sleeping
	 * functions */
	systick_timer_on(1); /* 1ms */
	systick_start();
}

/* Define our fault handler. This one is not mandatory, the dummy fault handler
 * will be used when it's not overridden here.
 * Note : The default one does a simple infinite loop. If the watchdog is deactivated
 * the system will hang.
 * An alternative would be to perform soft reset of the micro-controller.
 */
void fault_info(const char* name, uint32_t len)
{
	uprintf(UART0, name);
	while (1);
}

/***************************************************************************** */

/**
 * Fonction principale
 * 
 * - Lance l'initialisation des pins
 * 
 * - Contient une boucle infinie
 *   qui exécute la procédure
 **/
int main(void)
{
	system_init();
	uart_on(UART0, 115200, NULL);
	adc_on(NULL);

	init_lights();

	while (1) {
		traffic_lights();
	}
	return 0;
}