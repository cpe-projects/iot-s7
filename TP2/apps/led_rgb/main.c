#include "core/system.h"
#include "core/systick.h"
#include "core/pio.h"
#include "drivers/adc.h"
#include "drivers/serial.h"
#include "extdrv/status_led.h"
#include "extdrv/ws2812.h"
#include "lib/stdio.h"
#include "drivers/gpio.h"

#define MODULE_VERSION    0x04
#define MODULE_NAME "RGB LED"

#define UART_BUFF_LEN 32

#define SELECTED_FREQ  FREQ_SEL_48MHz

/***************************************************************************** */

/**
 * Définition des PINs utilisés pour les LEDs de status(requis)
 **/
const struct pio status_led_green = LPC_GPIO_1_4;
const struct pio status_led_red = LPC_GPIO_1_5;

const struct pio_config common_pins[] = {
	/* UART 0 */
	{ LPC_UART0_RX_PIO_0_1,  LPC_IO_DIGITAL },
	{ LPC_UART0_TX_PIO_0_2,  LPC_IO_DIGITAL },
	/* I2C 0 */
	{ LPC_I2C0_SCL_PIO_0_10, (LPC_IO_DIGITAL | LPC_IO_OPEN_DRAIN_ENABLE) },
	{ LPC_I2C0_SDA_PIO_0_11, (LPC_IO_DIGITAL | LPC_IO_OPEN_DRAIN_ENABLE) },
	/* */
	{ LPC_GPIO_0_23, LPC_IO_DIGITAL }, 
	ARRAY_LAST_PIO,
};

/**
 * Définition des PINs utilisés
 **/
const struct pio rgb_led = LPC_GPIO_0_23; 

/**
 * Variables globales 
 * pour gérer les couleurs
 **/
static volatile uint8_t num_value = 0;
static volatile uint16_t rgb_values[3] = {0,0,0};

/**
 * Callback appelée lors de la réception
 * des données sur la liaison UART
 **/
static volatile uint32_t cc_tx = 0;
static volatile uint16_t cc_tx_buff[UART_BUFF_LEN];
static volatile uint8_t cc_ptr = 0;
void handle_uart_cmd(uint8_t c)
{
	if (cc_ptr < UART_BUFF_LEN-1) {
		cc_tx_buff[cc_ptr++] = c;
	} else {
		cc_ptr = 0;
	}
	if ((c == '\n') || (c == '\r')) {
		cc_tx_buff[cc_ptr] = '\0';
		cc_ptr = 0;
		cc_tx = 1;
	}
}

/**
 * Envoie la couleur à afficher
 **/
void send_color(uint16_t red, uint16_t green, uint16_t blue) {
	ws2812_set_pixel(0, red, green, blue);
	ws2812_send_frame(0);
}

/**
 * Eteint les LED
 **/
void reset_color() {	
	send_color(0, 0, 0);
}

/**
 * Envoie via la liaison série les couleurs de la LED
 * au format décimal r,g,b
 **/
void send_color_to_uart() {
	uprintf(UART0, "Hey couleur %d,%d,%d! \n", rgb_values[0], rgb_values[1], rgb_values[2]);
}

/**
 * Change la couleur de la LED en 
 * fonciton des valeurs reçues
 **/
void set_color() {
	send_color(rgb_values[0], rgb_values[1], rgb_values[2]);
}

/***************************************************************************** */

/**
 * Initialise les pins GPIO
 **/
void app_init() {
	ws2812_config(&rgb_led);
	cc_tx_buff[0] = '\0';

	reset_color();
}

/***************************************************************************** */

/**
 * Initialisation obligatoire
 * pour permettre au système de
 * foncitonner correctement
 **/
void system_init()
{
	/* Stop the watchdog */
	startup_watchdog_disable(); /* Do it right now, before it gets a chance to break in */
	system_brown_out_detection_config(0); /* No ADC used */
	system_set_default_power_state();
	clock_config(SELECTED_FREQ);
	set_pins(common_pins);
	gpio_on();
	status_led_config(&status_led_green, &status_led_red);
	/* System tick timer MUST be configured and running in order to use the sleeping
	 * functions */
	systick_timer_on(1); /* 1ms */
	systick_start();

	uart_on(UART0, 115200, handle_uart_cmd);
}

/* Define our fault handler. This one is not mandatory, the dummy fault handler
 * will be used when it's not overridden here.
 * Note : The default one does a simple infinite loop. If the watchdog is deactivated
 * the system will hang.
 * An alternative would be to perform soft reset of the micro-controller.
 */
void fault_info(const char* name, uint32_t len)
{
	uprintf(UART0, name);
	while (1);
}


static volatile uint16_t atoi(volatile uint16_t *str)
{
    uint16_t res = 0; // Initialize result
  
    // Iterate through all characters of input string and
    // update result
    for (int i = 0; str[i] != '\0'; ++i) {
    	if(str[i] >= '0' && str[i] <= '9')
        	res = res*10 + str[i] - '0';
    }
  
    // return result.
    return res;
}

/***************************************************************************** */

/**
 * Fonction principale
 * 
 * - Lance l'initialisation des pins
 * 
 * - Contient une boucle infinie
 *   qui exécute la procédure
 **/
int main(void)
{
	system_init();
	adc_on(NULL);

	uprintf(UART0, "system_init done\n");

	app_init();

	uprintf(UART0, "app_init done\n");


	while (1) {

		if (cc_tx == 1) {
			/* 
				- Lorsqu'on reçoit un nombre (après appui sur Entrée)
				- A chaque appui sur entrée, on stocke la valeur (R, G ou B)
				- Lorsqu'on a reçu 3 valeurs : on change la couleur de la LED
			*/

			rgb_values[num_value] = atoi(cc_tx_buff);
			num_value++;
			cc_tx = 0;

			if(num_value == 3) {
				set_color();
				send_color_to_uart();
				num_value = 0;
			}

		}

	}
	return 0;
}