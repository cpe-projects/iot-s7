#include "core/system.h"
#include "core/systick.h"
#include "core/pio.h"
#include "drivers/adc.h"
#include "drivers/serial.h"
#include "drivers/gpio.h"
#include "drivers/ssp.h"
#include "extdrv/status_led.h"
#include "extdrv/ws2812.h"
#include "extdrv/cc1101.h"
#include "lib/stdio.h"

#define MODULE_VERSION    0x04
#define MODULE_NAME "RGB LED RF"

#define UART_BUFF_LEN 32
#define RF_BUFF_LEN 64

#define CLIENT_ADDR 227
#define SERVER_ADDR 230

#define SELECTED_FREQ  FREQ_SEL_48MHz

/***************************************************************************** */

/**
 * Définition des PINs utilisés pour les LEDs de status(requis)
 **/
const struct pio status_led_green = LPC_GPIO_1_4;
const struct pio status_led_red = LPC_GPIO_1_5;

const struct pio_config common_pins[] = {
	/* UART 0 */
	{ LPC_UART0_RX_PIO_0_1,  LPC_IO_DIGITAL },
	{ LPC_UART0_TX_PIO_0_2,  LPC_IO_DIGITAL },
	/* I2C 0 */
	{ LPC_I2C0_SCL_PIO_0_10, (LPC_IO_DIGITAL | LPC_IO_OPEN_DRAIN_ENABLE) },
	{ LPC_I2C0_SDA_PIO_0_11, (LPC_IO_DIGITAL | LPC_IO_OPEN_DRAIN_ENABLE) },
	/* SPI */
	{ LPC_SSP0_SCLK_PIO_0_14, LPC_IO_DIGITAL },
	{ LPC_SSP0_MOSI_PIO_0_17, LPC_IO_DIGITAL },
	{ LPC_SSP0_MISO_PIO_0_16, LPC_IO_DIGITAL },
	/* */
	{ LPC_GPIO_0_23, LPC_IO_DIGITAL }, 
	ARRAY_LAST_PIO,
};

/**
 * Définition des PINs utilisés
 **/
const struct pio rgb_led = LPC_GPIO_0_23; 

const struct pio cc1101_gdo0 = LPC_GPIO_0_6;
const struct pio cc1101_cs_pin = LPC_GPIO_0_15;
const struct pio cc1101_miso_pin = LPC_SSP0_MISO_PIO_0_16;

struct SensorData{
	uint32_t temp;
	uint32_t humidity;
	uint16_t lux;
};

static uint8_t rf_specific_settings[] = {
	CC1101_REGS(gdo_config[2]), 0x07, /* GDO_0 - Assert on CRC OK | Disable temp sensor */
	CC1101_REGS(gdo_config[0]), 0x2E, /* GDO_2 - FIXME : do something usefull with it for tests */
	CC1101_REGS(pkt_ctrl[0]), 0x0F, /* Accept all sync, CRC err auto flush, Append, Addr check and Bcast */
	CC1101_REGS(radio_stm[1]), 0x3F, /* CCA mode "if RSSI below threshold", Stay in RX, Go to RX (page 81) */
	CC1101_REGS(agc_ctrl[1]), 0x20, /* LNA 2 gain decr first, Carrier sense relative threshold set to 10dB increase in RSSI value */
};

/**
 * Variables globales 
 **/
static volatile uint16_t order_data[3] = {'T', 'H', 'L'}; // Par défaut THL

/**
 * Callback appelée lors de la réception
 * des données sur la liaison UART
 **/
static volatile uint32_t cc_tx = 0;
static volatile uint16_t cc_tx_buff[UART_BUFF_LEN];
static volatile uint8_t cc_ptr = 0;
void handle_uart_cmd(uint8_t c)
{
	if (cc_ptr < UART_BUFF_LEN-1) {
		cc_tx_buff[cc_ptr++] = c;
	} else {
		cc_ptr = 0;
	}
	if ((c == '\n') || (c == '\r')) {
		cc_tx_buff[cc_ptr] = '\0';
		cc_ptr = 0;
		cc_tx = 1;
	}
}

/*****************************************************************************/

void send_on_RF()
{
	uint8_t cc_tx_data[RF_BUFF_LEN + 2];

	cc_tx_data[0] = 5; // Taille du tableau - 1
	cc_tx_data[1] = CLIENT_ADDR; // @
	cc_tx_data[2] = 'C'; // R
	cc_tx_data[3] = order_data[0]; // 
	cc_tx_data[4] = order_data[1]; // 
	cc_tx_data[5] = order_data[2]; // 

	//uprintf(UART0, "RF: sending: adrr-> %d  type-> %c message-> %c, %c, %c ..\n", cc_tx_data[1], cc_tx_data[2], cc_tx_data[3], cc_tx_data[4], cc_tx_data[5]);

	if (cc1101_tx_fifo_state() != 0) {
		cc1101_flush_tx_fifo();
	}

	cc1101_send_packet(cc_tx_data, 6); // data, taille du tableau
}

static volatile int check_rx = 0;
void rf_rx_calback(uint32_t gpio)
{
	check_rx = 1;
}

void handle_RF(void)
{
	uint8_t data[RF_BUFF_LEN + 2];
	int8_t ret = 0;
	uint8_t status = 0;
	 
	/* Check for received packet (and get it if any) */
	ret = cc1101_receive_packet(data, RF_BUFF_LEN, &status);
	/* Go back to RX mode */
	cc1101_enter_rx_mode();

	if(data[1] != SERVER_ADDR) {
		//uprintf(UART0, "RF: Broadcast msg received\n");
	} else {
		struct SensorData *sensorData;
		sensorData = (struct SensorData*) &(data[3]);

		//uprintf(UART0, "RF: ret:%d, st: %d.\n", ret, status);

		uint32_t temp = sensorData->temp;
	    uint32_t humidity = sensorData->humidity;
	    uint16_t lux = sensorData->lux;


		if(data[2] == 'D'){
			uprintf(UART0, "[%d,%d,%d]\n", temp, humidity, lux);
		}
	}

}

/***************************************************************************** */

void rf_config(void)
{
	config_gpio(&cc1101_gdo0, LPC_IO_MODE_PULL_UP, GPIO_DIR_IN, 0);
	cc1101_init(0, &cc1101_cs_pin, &cc1101_miso_pin); /* ssp_num, cs_pin, miso_pin */
	/* Set default config */
	cc1101_config();
	/* And change application specific settings */
	cc1101_update_config(rf_specific_settings, sizeof(rf_specific_settings));
	cc1101_set_address(SERVER_ADDR);
	set_gpio_callback(rf_rx_calback, &cc1101_gdo0, EDGE_RISING);
}

/**
 * Initialise les pins GPIO
 **/
void app_init() {
	ws2812_config(&rgb_led);
	cc_tx_buff[0] = '\0';

	rf_config();
}

/***************************************************************************** */

/**
 * Initialisation obligatoire
 * pour permettre au système de
 * foncitonner correctement
 **/
void system_init()
{
	/* Stop the watchdog */
	startup_watchdog_disable(); /* Do it right now, before it gets a chance to break in */
	system_brown_out_detection_config(0); /* No ADC used */
	system_set_default_power_state();
	clock_config(SELECTED_FREQ);
	set_pins(common_pins);
	gpio_on();
	status_led_config(&status_led_green, &status_led_red);
	/* System tick timer MUST be configured and running in order to use the sleeping
	 * functions */
	systick_timer_on(1); /* 1ms */
	systick_start();

	uart_on(UART0, 115200, handle_uart_cmd);
}

/* Define our fault handler. This one is not mandatory, the dummy fault handler
 * will be used when it's not overridden here.
 * Note : The default one does a simple infinite loop. If the watchdog is deactivated
 * the system will hang.
 * An alternative would be to perform soft reset of the micro-controller.
 */
void fault_info(const char* name, uint32_t len)
{
	uprintf(UART0, name);
	while (1);
}

/***************************************************************************** */

/**
 * Fonction principale
 * 
 * - Lance l'initialisation des pins
 * 
 * - Contient une boucle infinie
 *   qui exécute la procédure
 **/
int main(void)
{
	system_init();
	adc_on(NULL);	
	ssp_master_on(0, LPC_SSP_FRAME_SPI, 8, 4*1000*1000); /* bus_num, frame_type, data_width, rate */

	uprintf(UART0, "system_init done\n");

	app_init();

	uprintf(UART0, "app_init done\n");

	while (1) {

		if (cc_tx == 1) {
			
			order_data[0] = cc_tx_buff[0];
			order_data[1] = cc_tx_buff[1];
			order_data[2] = cc_tx_buff[2];

			cc_tx = 0;

			uprintf(UART0, "Order should be %c, %c, %c\n", order_data[0], order_data[1], order_data[2]);
			send_on_RF();
			uprintf(UART0, "sent\n");

		}

		if(check_rx == 1) {
			handle_RF();
			check_rx = 0;
		}

		usleep(50);

	}
	return 0;
}
