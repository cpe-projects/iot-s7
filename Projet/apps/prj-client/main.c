#include "core/system.h"
#include "core/systick.h"
#include "core/pio.h"
#include "drivers/adc.h"
#include "drivers/serial.h"
#include "drivers/gpio.h"
#include "drivers/ssp.h"
#include "drivers/i2c.h"
#include "extdrv/status_led.h"
#include "extdrv/ws2812.h"
#include "extdrv/cc1101.h"
#include "extdrv/bme280_humidity_sensor.h"
#include "extdrv/tsl256x_light_sensor.h"
#include "extdrv/ssd130x_oled_driver.h"
#include "extdrv/ssd130x_oled_buffer.h"
#include "lib/font.h"
#include "lib/stdio.h"

#define MODULE_VERSION    0x04
#define MODULE_NAME "RGB LED RF"

#define UART_BUFF_LEN 32
#define RF_BUFF_LEN 64

#define CLIENT_ADDR 227
#define SERVER_ADDR 230

#define SELECTED_FREQ  FREQ_SEL_48MHz

/**
 * Sensors
 **/
/***************************************************************************** */

/**
 * Définition des PINs utilisés pour les LEDs de status(requis)
 **/
const struct pio status_led_green = LPC_GPIO_1_4;
const struct pio status_led_red = LPC_GPIO_1_5;

const struct pio_config common_pins[] = {
	/* UART 0 */
	{ LPC_UART0_RX_PIO_0_1,  LPC_IO_DIGITAL },
	{ LPC_UART0_TX_PIO_0_2,  LPC_IO_DIGITAL },
	/* I2C 0 */
	{ LPC_I2C0_SCL_PIO_0_10, (LPC_IO_DIGITAL | LPC_IO_OPEN_DRAIN_ENABLE) },
	{ LPC_I2C0_SDA_PIO_0_11, (LPC_IO_DIGITAL | LPC_IO_OPEN_DRAIN_ENABLE) },
	/* SPI */
	{ LPC_SSP0_SCLK_PIO_0_14, LPC_IO_DIGITAL },
	{ LPC_SSP0_MOSI_PIO_0_17, LPC_IO_DIGITAL },
	{ LPC_SSP0_MISO_PIO_0_16, LPC_IO_DIGITAL },
	/* */
	{ LPC_GPIO_0_23, LPC_IO_DIGITAL }, 
	ARRAY_LAST_PIO,
};

/**
 * Définition des PINs utilisés
 **/
const struct pio rgb_led = LPC_GPIO_0_23; 

const struct pio cc1101_gdo0 = LPC_GPIO_0_6;
const struct pio cc1101_cs_pin = LPC_GPIO_0_15;
const struct pio cc1101_miso_pin = LPC_SSP0_MISO_PIO_0_16;

static uint8_t rf_specific_settings[] = {
	CC1101_REGS(gdo_config[2]), 0x07, /* GDO_0 - Assert on CRC OK | Disable temp sensor */
	CC1101_REGS(gdo_config[0]), 0x2E, /* GDO_2 - FIXME : do something usefull with it for tests */
	CC1101_REGS(pkt_ctrl[0]), 0x0F, /* Accept all sync, CRC err auto flush, Append, Addr check and Bcast */
	CC1101_REGS(radio_stm[1]), 0x3F, /* CCA mode "if RSSI below threshold", Stay in RX, Go to RX (page 81) */
	CC1101_REGS(agc_ctrl[1]), 0x20, /* LNA 2 gain decr first, Carrier sense relative threshold set to 10dB increase in RSSI value */
};

/**
 * Variables globales 
 * pour gérer les données
 **/
static volatile uint8_t num_value = 0;

struct SensorData {
	uint32_t temp;
	uint32_t humidity;
	uint16_t lux;
};

// Offset 0<=> position Température, 1<=>Luminosité, 2<=>Hygrométrie
static volatile uint16_t data_order[3] = {0,1,2};

/**
 * Callback appelée lors de la réception
 * des données sur la liaison UART
 **/
static volatile uint32_t cc_tx = 0;
static volatile uint16_t cc_tx_buff[UART_BUFF_LEN];
static volatile uint8_t cc_ptr = 0;
void handle_uart_cmd(uint8_t c)
{
	if (cc_ptr < UART_BUFF_LEN-1) {
		cc_tx_buff[cc_ptr++] = c;
	} else {
		cc_ptr = 0;
	}
	if ((c == '\n') || (c == '\r')) {
		cc_tx_buff[cc_ptr] = '\0';
		cc_ptr = 0;
		cc_tx = 1;
	}
}

/*****************************************************************************/

void send_on_RF(char cmd, int temp, int humidity, int lux)
{
	uint8_t cc_tx_data[RF_BUFF_LEN + 2];

	struct SensorData *data;
	data = (struct SensorData*) &(cc_tx_data[3]);

	data->temp = temp;
	data->humidity = humidity;
	data->lux = lux;

	// +10 car uint16 <-> 2 octet et uint32 <-> 4 octets

	cc_tx_data[0] = 3 + 10; // Taille du tableau - 1
	cc_tx_data[1] = SERVER_ADDR; // @
	cc_tx_data[2] = cmd; // Commande

	uprintf(UART0, "RF: To @%d <%c> %d,%d,%d\n", cc_tx_data[1], cc_tx_data[2], data->temp, data->humidity, data->lux);

	if (cc1101_tx_fifo_state() != 0) {
		cc1101_flush_tx_fifo();
	}

	cc1101_send_packet(cc_tx_data, 4 + 10); // data, taille du tableau
}

static volatile int check_rx = 0;
void rf_rx_calback(uint32_t gpio)
{
	check_rx = 1;
}

void handle_RF(void)
{
	uint8_t data[RF_BUFF_LEN];
	int8_t ret = 0;
	uint8_t status = 0;

	/* Check for received packet (and get it if any) */
	ret = cc1101_receive_packet(data, RF_BUFF_LEN, &status);
	/* Go back to RX mode */
	cc1101_enter_rx_mode();

	if(data[1] != CLIENT_ADDR) {
		uprintf(UART0, "RF: Broadcast msg received\n");
	} else {

		uprintf(UART0, "RF: From @%d <%c> %c,%c,%c (ret:%d; status:%d)\n", data[1], data[2], data[3], data[4], data[5], ret, status);

		if(data[2] == 'C') {
			int i=0;
			for (i = 0; i < 3; i++)
			{
				if(data[i+3] == 'T')
					data_order[0] = i;
				if(data[i+3] == 'L')
					data_order[1] = i;
				if(data[i+3] == 'H')
					data_order[2] = i;
			}
		}

	}

}

/***************************************************************************** */
/***************************************************************************** */
/* Luminosity */

/* Note : These are 8bits address */
#define TSL256x_ADDR   0x52 /* Pin Addr Sel (pin2 of tsl256x) connected to GND */
struct tsl256x_sensor_config tsl256x_sensor = {
	.bus_num = I2C0,
	.addr = TSL256x_ADDR,
	.gain = TSL256x_LOW_GAIN,
	.integration_time = TSL256x_INTEGRATION_100ms,
	.package = TSL256x_PACKAGE_T,
};

void lux_config(int uart_num)
{
	int ret = 0;
	ret = tsl256x_configure(&tsl256x_sensor);
	if (ret != 0) {
		uprintf(uart_num, "Lux config error: %d\n", ret);
	}
}

void get_lux(int uart_num, int *luxPtr)
{
	uint16_t comb = 0, ir = 0;
	uint32_t lux = 0;
	int ret = 0;

	ret = tsl256x_sensor_read(&tsl256x_sensor, &comb, &ir, &lux);
	if (ret != 0) {
		uprintf(uart_num, "Lux read error: %d\n", ret);
	} else {
		*luxPtr = lux;	

		uprintf(uart_num, "Lux: %d  (Comb: 0x%04x, IR: 0x%04x)\n", lux, comb, ir);
	}
}

/***************************************************************************** */
/* BME280 Sensor */

/* Note : 8bits address */
#define BME280_ADDR   0xEC
struct bme280_sensor_config bme280_sensor = {
	.bus_num = I2C0,
	.addr = BME280_ADDR,
	.humidity_oversampling = BME280_OS_x16,
	.temp_oversampling = BME280_OS_x16,
	.pressure_oversampling = BME280_OS_x16,
	.mode = BME280_NORMAL,
	.standby_len = BME280_SB_62ms,
	.filter_coeff = BME280_FILT_OFF,
};

void bme_config(int uart_num)
{
	int ret = 0;

	ret = bme280_configure(&bme280_sensor);
	if (ret != 0) {
		uprintf(uart_num, "Sensor config error: %d\n", ret);
	}
}

void get_bme(int uart_num, int *tempPtr, int *humidityPtr)
{
	uint32_t pressure = 0, temp = 0;
	uint16_t humidity = 0;
	int ret = 0;

	ret = bme280_sensor_read(&bme280_sensor, &pressure, &temp, &humidity);
	if (ret != 0) {
		uprintf(uart_num, "Sensor read error: %d\n", ret);
	} else {
		int comp_temp = 0;
		uint32_t comp_humidity = 0;

		comp_temp = bme280_compensate_temperature(&bme280_sensor, temp) / 10;
		comp_humidity = bme280_compensate_humidity(&bme280_sensor, humidity) / 10;

		*tempPtr = comp_temp;
		*humidityPtr = comp_humidity;

		uprintf(uart_num, "PT: %d,%02d degC, H: %d,%d rH\n",
				comp_temp / 10,  (comp_temp > 0) ? (comp_temp % 10) : ((-comp_temp) % 10),
				comp_humidity / 10, comp_humidity % 10);
	}
}

/***************************************************************************** */
/* Oled Display */

#define DISPLAY_ADDR   0x78
static uint8_t gddram[ 4 + GDDRAM_SIZE ];
struct oled_display display = {
	.bus_type = SSD130x_BUS_I2C,
	.address = DISPLAY_ADDR,
	.bus_num = I2C0,
	.charge_pump = SSD130x_INTERNAL_PUMP,
	.video_mode = SSD130x_DISP_NORMAL,
	.contrast = 128,
	.scan_dir = SSD130x_SCAN_BOTTOM_TOP,
	.read_dir = SSD130x_RIGHT_TO_LEFT,
	.display_offset_dir = SSD130x_MOVE_TOP,
	.display_offset = 4,
  .gddram = gddram,
};

#define ROW(x)   VERTICAL_REV(x)
DECLARE_FONT(font);

void display_char(uint8_t line, uint8_t col, uint8_t c)
{
	uint8_t tile = (c > FIRST_FONT_CHAR) ? (c - FIRST_FONT_CHAR) : 0;
	uint8_t* tile_data = (uint8_t*)(&font[tile]);
	ssd130x_buffer_set_tile(gddram, col, line, tile_data);
}

int display_line(uint8_t line, uint8_t col, char* text)
{
	int len = strlen((char*)text);
	int i = 0;

	for (i = 0; i < len; i++) {
		uint8_t tile = (text[i] > FIRST_FONT_CHAR) ? (text[i] - FIRST_FONT_CHAR) : 0;
		uint8_t* tile_data = (uint8_t*)(&font[tile]);
		ssd130x_buffer_set_tile(gddram, col++, line, tile_data);
		if (col >= (SSD130x_NB_COL / 8)) {
			col = 0;
			line++;
			if (line >= SSD130x_NB_PAGES) {
				return i;
			}
		}
	}
	return len;
}

void set_oled_line(uint8_t line, uint8_t col, char* text) {
	display_line(line, col, text);
	ssd130x_display_full_screen(&display);
} 

/***************************************************************************** */

void rf_config(void)
{
	config_gpio(&cc1101_gdo0, LPC_IO_MODE_PULL_UP, GPIO_DIR_IN, 0);
	cc1101_init(0, &cc1101_cs_pin, &cc1101_miso_pin); /* ssp_num, cs_pin, miso_pin */
	/* Set default config */
	cc1101_config();
	/* And change application specific settings */
	cc1101_update_config(rf_specific_settings, sizeof(rf_specific_settings));
	set_gpio_callback(rf_rx_calback, &cc1101_gdo0, EDGE_RISING);

	cc1101_set_address(CLIENT_ADDR);
}

void screen_init() {
	/* Configure and start display */
	ssd130x_display_on(&display);
	/* Erase screen with lines, makes it easier to know things are going right */
	ssd130x_buffer_set(gddram, 0x10);
	int ret = ssd130x_display_full_screen(&display);

	if(ret == 0)
		uprintf(UART0, "Display error: %d", ret);

}

/**
 * Initialise les pins GPIO
 **/
void app_init() {
	ws2812_config(&rgb_led);
	cc_tx_buff[0] = '\0';

	rf_config();

	bme_config(UART0);
	lux_config(UART0);
	screen_init();
}

/***************************************************************************** */
/**
 * Initialisation obligatoire
 * pour permettre au système de
 * foncitonner correctement
 **/
void system_init()
{
	/* Stop the watchdog */
	startup_watchdog_disable(); /* Do it right now, before it gets a chance to break in */
	system_brown_out_detection_config(0); /* No ADC used */
	system_set_default_power_state();
	clock_config(SELECTED_FREQ);
	set_pins(common_pins);
	gpio_on();
	status_led_config(&status_led_green, &status_led_red);
	/* System tick timer MUST be configured and running in order to use the sleeping
	 * functions */
	systick_timer_on(1); /* 1ms */
	systick_start();

	uart_on(UART0, 115200, handle_uart_cmd);
	i2c_on(I2C0, I2C_CLK_100KHz, I2C_MASTER);
}

/* Define our fault handler. This one is not mandatory, the dummy fault handler
 * will be used when it's not overridden here.
 * Note : The default one does a simple infinite loop. If the watchdog is deactivated
 * the system will hang.
 * An alternative would be to perform soft reset of the micro-controller.
 */
void fault_info(const char* name, uint32_t len)
{
	uprintf(UART0, name);
	while (1);
}

/***************************************************************************** */

char disp_buffer[128] = "";
void display_data(int temp, int humidity, int lux) {

	for(int i = 0; i < 3; i++) {
		if(data_order[0] == i)
			snprintf(disp_buffer, 128, "Temp: %d,%02d dC ", temp / 10,  (temp > 0) ? (temp % 10) : ((-temp) % 10));

		if(data_order[1] == i)
			snprintf(disp_buffer, 128, "Lux:  %04d  lux", lux);

		if(data_order[2] == i)
			snprintf(disp_buffer, 128, "Hygr: %d,%d rH ", humidity / 10, humidity % 10);

		set_oled_line(i+3, 1, disp_buffer);
	}

}

/**
 * Fonction principale
 * 
 * - Lance l'initialisation des pins
 * 
 * - Contient une boucle infinie
 *   qui exécute la procédure
 **/
int main(void)
{
	system_init();
	adc_on(NULL);	
	ssp_master_on(0, LPC_SSP_FRAME_SPI, 8, 4*1000*1000); /* bus_num, frame_type, data_width, rate */

	uprintf(UART0, "system_init done\n");

	app_init();

	uprintf(UART0, "app_init done\n");
	
	set_oled_line(0, 6, "METEO");

	int temp = 0, humidity = 0, lux = 0;

	while (1) {

		get_bme(UART0, &temp, &humidity);
		get_lux(UART0, &lux);

		send_on_RF('D', temp, humidity, lux);
		display_data(temp, humidity, lux);

		if(check_rx == 1) {
			handle_RF();
			check_rx = 0;
		}

		usleep(500);

	}
	return 0;
}